import { useState, useEffect } from 'react';
import jsonPlaceHolder from '../apis/jsonPlaceHolder.js';


//REUSABLE LOGIC WHICH CAN BE USED IN DIFFERENT FUNCTIONAL COMPONENETS
const useResources = (resource) =>{
    const [resources, setResources] = useState([]);     

    const fetchResource = async (resource) =>{
        const response = await jsonPlaceHolder.get(`/${resource}`);
        setResources(response.data);
    };

    useEffect(() => {
        fetchResource(resource);
    }, [resource]);

    return resources; 
};

export default useResources;